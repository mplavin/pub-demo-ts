import { Color, Round, Shape } from './index.types';

export const sum = (a: number, b: number) => {
  if ('development' === process.env.NODE_ENV) {
    console.log('boop');
  }
  return a + b;
};

export class Circle implements Round {
  private value: number;

  constructor(public radius: number) {
    this.radius = radius;
    this.value = radius;
  }

  square() {
    return (Math.PI * this.radius) ^ 2;
  }

  getRadius() {
    return this.radius;
  }

  getValue() {
    return this.value;
  }
}

export class Red implements Color {
  constructor(private color: string) {}

  getColor() {
    return this.color;
  }
}

export class Figure implements Shape {
  constructor(private shape: string) {}

  getShape() {
    return this.shape;
  }
}

export const circle = new Circle(2);
