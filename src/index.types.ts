export declare const sum: (a: number, b: number) => number;

export interface Round {
  radius: number;
  square: () => number;
  getRadius: () => number;
}

export interface Color {}

export interface Shape {}
